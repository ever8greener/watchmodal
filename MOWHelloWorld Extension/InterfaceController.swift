//
//  InterfaceController.swift
//  MOWHelloWorld Extension
//
//  Created by artist on 4/3/16.
//  Copyright © 2016 artist. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    override init(){
        super.init()
        print("jkjk")
    }
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        print("awakeWithContext")
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("willActivate")
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        print("didDeactivete")
    }
    override func contextForSegueWithIdentifier(segueIdentifier: String) -> AnyObject? {
        
        if segueIdentifier == "idPushNav"{
            return ["segue":"hierarchical", "data":"패쓰쓰루"]
        }else if segueIdentifier == "idModalPaged"{
            return ["segue":"pagebased", "data":"페이지"]
        }else{
            
            return ["segue":"","data":""]
        }
    }
    
}
