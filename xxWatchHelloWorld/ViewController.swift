//
//  ViewController.swift
//  xxWatchHelloWorld
//
//  Created by artist on 4/3/16.
//  Copyright © 2016 artist. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        // optional 존재의 이유!!!!!
        // ref: http://chanlee.github.io/2015/06/14/Introduce-Swift-Optional/
        
        var lastName:String? =  "Johnson"
        let firstName:String  = "Tom"
        let message = firstName + lastName!  // ok.
        print (message)
        
        
        var lastName2:String? =  nil
        let firstName2:String = "Tom"
        let message2 = firstName2 + lastName2!  // compile error. 옵셔널 존재의 이유
        //fatal error: unexpectedly found nil while unwrapping an Optional value
        print (message)
        
        let message3 = "something new 2nd merge test"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        //memory test
        let todoin :String? = "my ID"
    }


}

